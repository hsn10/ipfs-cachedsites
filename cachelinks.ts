import { create } from 'kubo-rpc-client'
import * as fs from 'node:fs'
import readline from 'node:readline'
import process from "node:process"
import last from 'it-last'

const MFS_TARGET='/cache/'

const ipfs = await create()

function processInputFile(input:string) {

   const lineReader = readline.createInterface({
      input: fs.createReadStream(input),
      terminal: false,
      crlfDelay: Infinity
   });

   lineReader.on('line', function (line) {
      const a = line.match(/\S+/g)
      if (a) {
                 const [ site, alias ] = a;
                 linkSite(site, alias);
      }
   });
}

function linkSite(site:string, alias:string|null) {
   const result = ipfs.name.resolve(site.startsWith('/ipns/')? site : '/ipns/'+site, { recursive: true, nocache: false } )
   last(result).then( (r) => {
      if (r) {
         const path = alias ? MFS_TARGET+alias : MFS_TARGET+site
         ipfs.files.rm(path, {recursive: true, flush: false}).catch( (_err) => {}).then( (_rm) => ipfs.files.cp(r,path, { flush: true, parents: true }) )
      }
   }
   )
}

function getCmdArguments() {
   for (const inputfile of (process.argv.length > 2 ? process.argv.slice(2) : ['sites.txt'])) {
      processInputFile(inputfile)
   }
}

getCmdArguments()
