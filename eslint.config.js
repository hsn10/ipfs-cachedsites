// eslint.config.js

import js from "@eslint/js";
import globals from "globals";

export default [
    js.configs.recommended,
{
    "files": [ "*.js", "*.ts" ],
    "rules": {
        "prefer-const": "error",
        "no-unused-vars": ["error", { "argsIgnorePattern": "^_" }]
    },
    "languageOptions": {
        "ecmaVersion": 2022,
        "sourceType" : "module",
        globals: {
            ...globals.node
        }
    }
}
];