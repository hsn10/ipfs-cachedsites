/** @type {import('ts-jest').JestConfigWithTsJest} */
export default {
   preset: 'ts-jest',
   testEnvironment: 'node',
   roots: [ "<rootDir>/tests/"],
   verbose: true,

   transform: {
     '^.+\\.[tj]sx?$': 'babel-jest'
   },
   testMatch: [
     "**/*_test.[tj]s"
     // '<rootDir>/tests/**/*_test.js'
     // "*.js",
     // "**/?(+[a-z]@(.|_))+(spec|test).[tj]s?(x)",
     // "**/*_test[tj]s?(x)",
   ],
   moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node']
};

